#!/usr/bin/env python3
# collect all the files from the fill and create a configuration file
import sys
from TICMeanTimeHandler import TICMeanTimeHandler

if __name__ == '__main__':
    handler = TICMeanTimeHandler(task='timing-val-cc',
                                 deps_tasks=['timing-reco-cc'],
                                 prev_input='timing-reco-cc',
                                 is_cc=True,
                                 is_rereco=False)

    ret = handler()

    sys.exit(ret)
