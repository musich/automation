#!/bin/bash

RUN_NUMBER=${1}
TAG=${2}
EOSDIR=${3}

# Get the payload for the given TAG; the latest payload at the time of the given run
iovs=`conddb list $TAG`

while IFS= read -r line; do
    iov=$(echo $line | awk '{print $1}')
    if [ $iov -gt $RUN_NUMBER ]
    then
        break
    else
        last_iov=$iov
        last_payload=$(echo $line | awk '{print $4}')
    fi
done <<< "$iovs"

echo for $RUN_NUMBER: use the payload $last_payload of IOV $last_iov

# make output directory for dat files and plots
mkdir -p $EOSDIR

# save the payload
echo $last_payload > $EOSDIR/tmppyload
