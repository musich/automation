from diagrams import Diagram, Cluster, Node
from diagrams.aws.compute import EC2Instance as singleJob
from diagrams.aws.compute import EC2Instances as multiJob
from diagrams.aws.management import OpsworksPermissions as locks
from diagrams.aws.iot import IotAnalyticsDataSet as file

with Diagram("Worflow: zee-fits", filename="zee-fits-wflow", show=False, direction="TB") as d:
    # s = singleJob("Single job", width='0.5')
    # m = multiJob("Batch jobs", width='0.5')
    # f = file("Workflow related files", width='0.5')
    with Cluster("zee-fits-reco", direction="LR"):
        Node("", width='1.', height='0', style='invisible')
        t0_lock = locks("T0ProcDatasetLock \n dataset=/AlCaZee-Fits \n stage=Repack", width='1.6')
        gt_lock = locks("CondDBLockGT: \n EcalLaserAPDPNRatiosRcd \n  EcalPedestalsRcd", width='1.6')
        reco_files = file("runreco.py \n alcanano_customize.py \n template.sub \n batch_script.sh")
        zee-fits_reco = multiJob("HTCHandlerByRunDBS \n dsetname=/AlCaZee-Fits/*/RAW")

    [t0_lock, gt_lock] >> zee-fits_reco
